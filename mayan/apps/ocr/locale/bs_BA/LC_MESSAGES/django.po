# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# Atdhe Tabaku <Atdhe617@gmail.com>, 2018
# www.ping.ba <jomer@ping.ba>, 2013
msgid ""
msgstr ""
"Project-Id-Version: Mayan EDMS\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-06-07 21:05-0400\n"
"PO-Revision-Date: 2018-08-09 10:54+0000\n"
"Last-Translator: Atdhe Tabaku <Atdhe617@gmail.com>\n"
"Language-Team: Bosnian (Bosnia and Herzegovina) (http://www.transifex.com/rosarior/mayan-edms/language/bs_BA/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: bs_BA\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"

#: apps.py:66 apps.py:125 apps.py:129 events.py:7 links.py:14 permissions.py:7
#: queues.py:7 settings.py:7
msgid "OCR"
msgstr "OCR"

#: apps.py:100
msgid "Document"
msgstr "Dokument"

#: apps.py:104
msgid "Added"
msgstr "Dodaj"

#: apps.py:108 models.py:57
msgid "Result"
msgstr "Rezultat"

#: events.py:10
msgid "Document version submitted for OCR"
msgstr "Verzija dokumenta dostavljena za OCR"

#: events.py:14
msgid "Document version OCR finished"
msgstr "OCR dokumentacije je završen"

#: forms.py:40
#, python-format
msgid "Page %(page_number)d"
msgstr "Strana %(page_number)d"

#: forms.py:48
msgid "Contents"
msgstr "Sadržaj"

#: links.py:18 links.py:21
msgid "Submit for OCR"
msgstr "Podnesite za OCR"

#: links.py:24
msgid "Setup OCR"
msgstr "Setup OCR"

#: links.py:29
msgid "OCR documents per type"
msgstr "OCR dokumenti po tipu"

#: links.py:33 links.py:37 views.py:116
msgid "OCR errors"
msgstr "OCR greške"

#: links.py:42
msgid "Download OCR text"
msgstr "Preuzmite OCR tekst"

#: models.py:18
msgid "Document type"
msgstr "Tip dokumenta"

#: models.py:22
msgid "Automatically queue newly created documents for OCR."
msgstr "Automatically queue newly created documents for OCR."

#: models.py:26
msgid "Document type settings"
msgstr "Podešavanja tipa dokumenta"

#: models.py:27
msgid "Document types settings"
msgstr "Podešavanja tipova dokumenata"

#: models.py:34
msgid "Document page"
msgstr "Strnica dokumenta"

#: models.py:36
msgid "Content"
msgstr "Sadržaj"

#: models.py:41
msgid "Document page OCR content"
msgstr "OCR sadržaj dokumenta"

#: models.py:42
msgid "Document pages OCR contents"
msgstr "Dokumenti stranica OCR sadržaj"

#: models.py:52
msgid "Document version"
msgstr "Verzija dokumenta"

#: models.py:55
msgid "Date time submitted"
msgstr "Datum podnošenja vremena"

#: models.py:61
msgid "Document version OCR error"
msgstr "Verzija dokumenta OCR greška"

#: models.py:62
msgid "Document version OCR errors"
msgstr "OCR greške o verziji dokumenta"

#: permissions.py:10
msgid "Submit documents for OCR"
msgstr "Predati dokumente za OCR"

#: permissions.py:14
msgid "View the transcribed text from document"
msgstr "Pogledajte transkribovani tekst iz dokumenta"

#: permissions.py:18
msgid "Change document type OCR settings"
msgstr "Promenite postavke OCR dokumenta tipa"

#: queues.py:9
msgid "Document version OCR"
msgstr "Verzija dokumenta OCR"

#: settings.py:12
msgid ""
"File path to poppler's pdftotext program used to extract text from PDF "
"files."
msgstr "Staza do popplerovog programa pdftotext za vađenje teksta iz PDF datoteka."

#: settings.py:19
msgid "Full path to the backend to be used to do OCR."
msgstr "Puni put do baze koja se koristi za izvršenje OCR-a."

#: settings.py:28
msgid "Set new document types to perform OCR automatically by default."
msgstr "Podesite nove tipove dokumenata koji automatski podrazumevaju OCR."

#: views.py:41
#, python-format
msgid "OCR result for document: %s"
msgstr "OCR rezultat za dokument: %s"

#: views.py:56
msgid "Submit the selected document to the OCR queue?"
msgid_plural "Submit the selected documents to the OCR queue?"
msgstr[0] "Podnesite odabrani dokument u OCR redosled?"
msgstr[1] "Pošaljite odabrane dokumente u redosled OCR-a?"
msgstr[2] "Pošaljite odabrane dokumente u redosled OCR-a?"

#: views.py:71
msgid "Submit all documents of a type for OCR"
msgstr "Pošaljite sve dokumente vrste za OCR"

#: views.py:85
#, python-format
msgid "%(count)d documents of type \"%(document_type)s\" added to the OCR queue."
msgstr "%(count)d dokumenti tipa \"%(document_type)s\" dodaje se u redosled OCR-a."

#: views.py:108
#, python-format
msgid "Edit OCR settings for document type: %s"
msgstr "Izmeni postavke OCR-a za tip dokumenta: %s"

#: views.py:134
#, python-format
msgid "OCR errors for document: %s"
msgstr "OCR greške za dokument: %s"
